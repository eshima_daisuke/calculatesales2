package jp.alhinc.eshima_daisuke.calsulate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales{
	public static void main(String[] args){
		Map<String, String> map = new HashMap<>();
		Map<String, Long> map2 = new HashMap<>();

		try {
			File branchLst = new File(args[0], "branch.lst");

			if (!branchLst.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			//(処理内容1-1)支店名ファイルの読み込み
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(branchLst));

				//(処理内容1-2)支店コードとそれに対応する支店名を保持する。
				String line;
				while ((line = br.readLine()) != null){
					String[] resultArray = line.split(",");

					//(エラー処理1-2)支店定義ファイルのフォーマットが不正な場合
					if(resultArray.length != 2 || resultArray[1].contains(" ") || !resultArray[0].matches("^[0-9]{3}$")) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					map.put(resultArray[0], resultArray[1]);
					map2.put(resultArray[0], 0L);
				}
			//(エラー処理1-1)支店定義ファイルが存在しない場合
			} finally {
				if (br != null) {
					br.close();
				}
			}


			//(処理内容2-2)売上表を読み込む
			File dir = new File(args[0]);
			File[] filelist = dir.listFiles();
			//(エラー処理2-1)filelistを昇順にする
			Arrays.sort(filelist);

			List<File> rcdFiles = new ArrayList<>();

			//(処理内容2-2)売上ファイルの数だけ繰り返し処理を行う
			for (File file : filelist) {
				//(処理内容2-1)拡張子rcd、数字８桁のファイルを指定する
				if(file.getName().matches("^[0-9]{8}.rcd$")) {
					rcdFiles.add(file);
				}
			}

			int min = Integer.parseInt(rcdFiles.get(0).getName().substring(0, 8));
			int max = Integer.parseInt(rcdFiles.get(rcdFiles.size() - 1).getName().substring(0, 8));

			if (min + rcdFiles.size() - 1 != max) {
				System.out.println( "売上ファイル名が連番になっていません");
				return;
			}

			BufferedReader br2 = null;
			for (File rcdFile : rcdFiles) {
				try {
					br2 = new BufferedReader(new FileReader(rcdFile));
					//(処理内容2-2)売上ファイルを１行ずつ読み込む
					String code = br2.readLine();
					Long sales = Long.parseLong(br2.readLine());

					//(エラー処理2-4)売上ファイルの中身が3行以上ある場合
					if(br2.readLine() != null) {
						System.out.println(rcdFile.getName() + "のフォーマットが不正です");
						return;
					}
					//(エラー処理2-3)支店に該当がなかった場合
					if(!map2.containsKey(code)) {
						System.out.println(rcdFile.getName() + "の支店コードが不正です");
						return;
					}
					//(処理内容2-2)読み取った売上金額をintに
					//(処理内容2-2)売上ファイルの支店番号、売上金額をmap2に入れる
					Long total = map2.get(code) + sales;
					//(エラー処理2-2)合計金額が10桁を超えた場合
					if(total.toString().length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					//(処理内容2-2)３つ目のmapをつくり支店名と売上表の支店ナンバーをくっつける
					map2.put(code, total);
				} finally {
					if(br2 != null) {
						br2.close();
					}
				}
			}
			//(処理内容3-1)支店別集計ファイルの作成
			PrintWriter pw = null;
			try {
				File file = new File(args[0], "branch.out");
				pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));
				for (String key : map.keySet()) {
					pw.println(key + "," + map.get(key) + "," + map2.get(key));
				}
			} finally {
				pw.close();
			}
		} catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}
